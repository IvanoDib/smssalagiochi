package com.makeitsimple.salagiochi.GiocoUno;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class Gioco1 extends Game {

	SpriteBatch batch;

	
	@Override
	public void create () {
		batch = new SpriteBatch();
		setScreen( new PlayScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}

}
