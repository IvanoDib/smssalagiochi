package com.makeitsimple.salagiochi;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.makeitsimple.salagiochi.GiocoUno.Gioco1;


public class AndroidLauncher extends AndroidApplication {

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

		//inizializza gioco1
		initialize(new Gioco1(), config);

		//inizializza gioco2
		//initialize(new Gioco2(), config);


		// ------------- TEST SERVER -------------
		//aggiungere permesso per utilizzare internet
		ServerHelper.InviaDati(this,"nickname100","100", "3");
	}


}
