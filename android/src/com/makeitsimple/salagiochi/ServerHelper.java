package com.makeitsimple.salagiochi;

import android.content.Context;
import android.util.Log;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class ServerHelper  {

    public static void InviaDati(Context context, final String nickname, final String score,final String idGioco){
        RequestQueue MyRequestQueue = Volley.newRequestQueue(context);
        String url = "http://sms1920salagiochi.altervista.org/scoreboard.php"; // url del server


        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //i valori nei log sono le print nel file .php del server
                Log.d("VOLLEY",response);
            }
        }, new Response.ErrorListener() { //listener per la gestione degli errori
            @Override
            public void onErrorResponse(VolleyError error) {
                //errore dal server
                Log.d("VOLLEY",error.toString());
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String > dati = new HashMap<>();
                dati.put("nickname", nickname);
                dati.put("score", score);
                dati.put("gioco",idGioco);
                return dati;
            }
        };


        MyRequestQueue.add(MyStringRequest);
    }

}
